import TodoView from "@/views/todo.view.vue";
import AuthView from "@/views/auth.view.vue";

import SignUp from "@/components/auth/sign-up.component.vue";
import SignIn from "@/components/auth/sign-in.component.vue";

import { createRouter, createWebHashHistory } from "vue-router";

const routes = [
  { path: "/", component: TodoView },
  {
    path: "/auth",
    component: AuthView,
    redirect: "/sign-in",
    children: [
      {
        path: "/sign-in",
        component: SignIn,
      },
      {
        path: "/sign-up",
        component: SignUp,
      },
    ],
  },
];

const router = createRouter({
  history: createWebHashHistory(),
  routes,
});

export default router;
